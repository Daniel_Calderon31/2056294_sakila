<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Login;

class CategoriaController extends Controller
{
    //Accion:Un metodo del controlador, contiene el codigo a ejecutar

    public function index() {
        
        if(Auth::check()){
        //seleccionar las categorias existentes
        $categorias = Categoria::paginate(5);
        //enviar la coleccion de categoria a una vista
        //y vamos a mostrar alli
        return view("categorias.index")->with("categorias", $categorias);
        }else {
            return redirect("login");
        }
    }
    //mostrar el formulario de crear
    public function create() {
        //echo "Formulario de Categoria";
        return view("categorias.new");
    }
    //llegar los datos desde el formulario
    //guardar la categoria en BD
    public function store(Request $r) {

        //Validacion
        //1.establecer las reglas de validacion

        $reglas = [
            "Categoria" => ["required", "alpha"]
        ];

        $mensajes = [
            "required" => "Campo obligatorio.",
            "alpha" => "Solo letras."
        ];

        //2. crear el objeto validador

        $validador = Validator::make($r->all(), $reglas, $mensajes);

        //3. Validar: metodo fails
        if ($validador->fails()) {
            //codigo para cuando falla
            return redirect("categorias/create")->withErrors($validador);
         }else {
            //codigo cuando la validacion es correcta
        }

        //almacena la informacion que viene desde formularios
        //Crear nueva categoria
        $categoria = new Categoria();
        //traer datos desde el formulario
        $categoria->name = $r->input("Categoria");
        //guardar la nueva categoria
        $categoria->save();
        //redireccionando con datos de sesion
        return redirect("categorias/create")->with("mensaje","categoria guardada");
    }
    public function edit($category_id) {
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);
        //mostrar la vista de actualizacion, llevando los datos de categoria
        return view("categorias.edit")->with("categoria",$categoria);
    }
    public function update() {

    }
}
