<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class LocationController extends Controller
{
    //Devolver los paises en formato json
    public function jsonciudades($idpais){
        $ciudades = Country::find($idpais)->ciudades()->get()->tojson();
        return $ciudades;
    }
}
