<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Pelicula;
use App\Categoria;

class PeliculaController extends Controller
{

    public function index(){
        //seleccionar las peliculas paginadas
        $peliculas = Pelicula::paginate(5);
        //
        return view("peliculas.index")->with("peliculas",$peliculas);
    }
    public function acordeon(){
        //consultar las categorias
        $categorias= Categoria::all();
        //llevar las categorias a las vista
        return view('peliculas.acordeon')->with('categorias',$categorias);

    }
    public function tabs(){
        //consultar las categorias
        $categorias= Categoria::all();
        //llevar las categorias a las vista
        return view('peliculas.tabs')->with('categorias',$categorias);

    }
}