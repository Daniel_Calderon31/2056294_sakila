<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class ClienteController extends Controller
{
    public function create(){

        //Cargar los paises a la vista
        $paises = Country::all();

        return view('clientes.new')->with("paises" , $paises);
    }
}
