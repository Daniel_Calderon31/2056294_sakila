<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Codec\TimestampFirstCombCodec;
use Ramsey\Uuid\Codec\TimestampLastCombCodec;

class Categoria extends Model
{
    protected $table="category";
    protected $primaryKey="category_id";
    public $timestamps=false;

    public function peliculas(){
        //belongs consultar tabla
        return $this->belongsToMany("App\Pelicula", 
                                    "film_category", 
                                    "category_id",
                                    "film_id");


    }
}
