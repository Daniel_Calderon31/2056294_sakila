<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Pelicula extends Model
{
    protected $table = "film";
    protected $primaryKey = "film_id";
    public $timestamps = false;


    public function peliculas(){                                                    
        return $this->belongsToMany("App\Pelicula", 
                                    "film_category", 
                                    "category_id",
                                    "film_id");
    } 

    //extender el modelo para relacionarlo con categorias

    public function categorias(){
        //belongs consultar tabla
        return $this->belongsToMany("App\Categoria", 
                                    "film_category",  
                                    "film_id",
                                    "category_id");


    }

    public function idioma(){
        return $this->belongsTo('App\Idioma', 
                                'language_id');
    }

    public function actores(){
        return $this->belongsToMany('App\Actor', 
                                    'film_actor',
                                    'film_id',
                                    'actor_id');
    }
}
