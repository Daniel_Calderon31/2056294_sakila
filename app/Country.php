<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table="country";
    protected $primaryKey = "country_id";
    public $timestamps = false;

    //extender el modelo para 1:m con ciudades
    public function ciudades(){
        
        return $this->hasMany("App\City",
                        "country_id");
    }

}
