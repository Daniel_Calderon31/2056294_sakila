<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script>
    $(document).ready(function(){
        
        //evento de seleccion de opcion
        $("#pais").change(function(){
            //limpiar el DOM interno del select
            $("#ciudad").html("");
            //seleccionar el id del pais
            let idpais= $("#pais").val();
            //realizar una petición desde javascript(asincronas)
            $.getJSON('../jsonciudades/' + idpais, function(ciudades){
                    //console.log(ciudades);
                    //recorrer cada ciudad
                    $.each(ciudades, function(indice, ciudad){
                        //console.log(ciudad);
                        //por cada ciudad crear un option y añadirlo al select
                    $("#ciudad").append("<option value="+ ciudad.city_id +">" + ciudad.city + "</option>")    
                    });
            });
        });

    });
</script>
    <title>Crear cliente</title>
</head>
<body>
    <form method="post" action="{{ url('clientes/store') }}" class="form-horizontal">
    @csrf    
        <fieldset>
        
        <!-- Form Name -->
        <legend>Nuevo Cliente</legend>
        
    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="textinput">Nombre:</label>  
        <div class="col-md-4">
        <input id="textinput" name="nombre_cliente" type="text" placeholder="" class="form-control input-md">
          
        </div>
      </div>

    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="textinput">Apellido:</label>  
        <div class="col-md-4">
        <input id="textinput" name="apellido_cliente" type="text" placeholder="" class="form-control input-md">
      
        </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
    <label class="col-md-4 control-label" for="textinput">Email:</label>  
    <div class="col-md-4">
    <input id="textinput" name="email_cliente" type="text" placeholder="" class="form-control input-md">
      
    </div>
  </div>
  
    <!-- Select Basic -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="selectbasic">País:</label>
        <div class="col-md-4">
        <select id="pais" name="pais_cliente" class="form-control">
            @foreach ($paises as $pais)
                <option value="{{$pais->country_id}}">{{ $pais->country }}</option>
            @endforeach
        </select>
        </div>
    </div>      


    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="selectbasic">Ciudad:</label>
      <div class="col-md-4">
        <select id="ciudad" name="ciudad_cliente" class="form-control">
        </select>
      </div>
    </div>
        

    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="textinput">Dirección:</label>  
        <div class="col-md-4">
        <input id="textinput" name="direccion_cliente" type="text" placeholder="" class="form-control input-md">      
        </div>
      </div>
    
    <!-- Multiple Checkboxes (inline) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="checkboxes">Activo:</label>
        <div class="col-md-4">
        <label class="checkbox-inline" for="checkboxes-0">
        <input type="checkbox" name="activo_cliente" id="checkboxes-0" value="1">
      </label>
    </div>
  </div>
  
   <!-- Button -->
   <div class="form-group">
    <label class="col-md-4 control-label" for=""></label>
    <div class="col-md-4">
      <button id="" name="" class="btn btn-primary">Guardar</button>
    </div>
  </div> 
    </fieldset>
    </form>
</body>
</html>