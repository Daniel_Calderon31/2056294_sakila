<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lista de Peliculas por categoria</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
</head>
<body>
 
<div id="accordion">
    @foreach ($categorias as $categoria)  
  <h3>{{ $categoria->name }}</h3>
  <div>
    <!-- tabla -->
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Descripción</th>
                <th>Clasificación</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categoria->peliculas()->get() as $pelicula)
            <tr>    
                <td class="text-success">{{$pelicula->title}}</td>
                <td>{{$pelicula->description}}</td>
                <td>{{$pelicula->rating}}</td>       
            </tr>
                @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Numero de Peliculas: {{$categoria->peliculas()->get()->count()}}</th>
            </tr>
        </tfoot>
    </table>
  </div>
  @endforeach
</div>
</body>
</html>