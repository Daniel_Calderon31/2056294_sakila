<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
    <title>Actualizar Categoria</title>
</head>
<body>
    <!-- Si la variable de sesion "mensaje" existe, la muestra-->
    @if(session("mensaje"))
    <p class="alert-success">{{ session("mensaje") }}</p>
    @endif
    <form class="form-horizontal" action="{{ url("categorias/store") }}" method="POST">
        @csrf
        <fieldset>
        
        <!-- Form Name -->
        <legend>Actualizar Categoria</legend>
        
        <!-- Prepended text-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="prependedtext">Nombre </label>
          <div class="col-md-4">
            <div class="input-group">
              <span class="input-group-addon">Categoria</span>
              <input id="prependedtext" name="Categoria" class="form-control" type="text" value="{{ $categoria->name }}">
            </div>
            <strong class="text-danger">{{ $errors->first("Categoria") }}</strong>            
          </div>
        </div>
        
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for=""></label>
          <div class="col-md-4">
            <button id="" name="" class="btn btn-info">Actualizar</button>
            <a class="btn btn-success" href="{{ url("categorias") }}">Regresar</a><br>
          </div>
        </div>
        
        </fieldset>
        </form> 
</body>
</html>